/*
 * Copyright 2014 Inemar Technology
 * email: opensource (at) inemar.net
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache license, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the license for the specific language governing permissions and
 * limitations under the license.
 */
package net.inemar.utility.log4j2_elastic;
 
import org.apache.logging.log4j.message.MapMessage;

public class FluidMapMessage extends MapMessage {

	private static final long serialVersionUID = 3885431241176011273L;
	
	
	public FluidMapMessage add(String key,String value) {
		this.put(key, value);
		return this;
	}
	
	public FluidMapMessage message(String value) {
		this.put("message", value);
		return this;
	}
	
	public FluidMapMessage msg(String value) {
		return this.message(value);
	}

}
